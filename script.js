const registerForm = document.querySelector("#registerForm")

registerForm.addEventListener(/*"Submit" -> Logical Error*/ "submit", (e) => {

	// e.preventDefault --> Syntax Error
	e.preventDefault()

	const username = document.querySelector("#username").value
	const email = document.querySelector("#email").value
	const mobileNo = document.querySelector(/*"#mobileNum" --> Logical Error*/ "#mobileNo").value
	const password1 = document.querySelector(/*"#password" --> Logical Error*/ "#password1").value
	const password2 = document.querySelector(/*"#password" --> Logical Error*/ "#password2").value

	let condition1 = false
	let condition2 = false
	// Let condition3 = false --> Syntax Error
	let condition3 = false

	if(password1.length < 6){
		// alerat("PASSWORD MUST BE AT LEAST 6 CHARACTERS LONG") --> Syntax Error
		alert("PASSWORD MUST BE AT LEAST 6 CHARACTERS LONG")
	}else{
		// condition1 == true --> Logical Error
		condition1 = true
	}

	if(password1.search(/[A-Z]/) < 1) {
	    alert("PASSWORD MUST INCLUDE AT LEAST ONE UPPERCASE CHARACTER")
	}else{
		// condition2 == true --> Logical Error
		condition2 = true
	}

	if(password1 !== password2){
		alert("PASSWORDS MUST MATCH")
	}else{
		// condition3 == true --> Logical Error
		condition3 = true
	}

	if(condition1 === true && condition2 === true /*$$ --> Syntax Error*/ && condition3 === true){
		// alert(`USER ${username} REGISTERED WITH EMAIL {email} AND MOBILE NUMBER ${mobileNo}`) -> Logical Error
		alert(`USER ${username} REGISTERED WITH EMAIL ${email} AND MOBILE NUMBER ${mobileNo}`)

		/*
		username == "" --> Logical Error
		email ==  "" --> Logical Error
		mobileNo == "" --> Logical Error
		password1 == "" --> Logical Error
		password2 == "" --> Logical Error
		*/
		username = ""
		email = "" 
		mobileNo = "" 
		password1 = ""
		password2 = ""
	}
})